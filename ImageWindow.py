import wx

#----------------------------------------------------------------------

class TestFrame(wx.Frame):
    def __init__(self, parent, args, title):
        # self.log = log
        wx.Frame.__init__(self, parent, -1, "Shaped Window",
                         style =
                           wx.FRAME_SHAPED
                         | wx.SIMPLE_BORDER
                         | wx.FRAME_NO_TASKBAR
                         | wx.STAY_ON_TOP
                         )

        self.hasShape = False
        self.delta = (0,0)

        self.Bind(wx.EVT_LEFT_DCLICK, self.OnDoubleClick)
        self.Bind(wx.EVT_LEFT_DOWN,   self.OnLeftDown)
        self.Bind(wx.EVT_LEFT_UP,     self.OnLeftUp)
        self.Bind(wx.EVT_MOTION,      self.OnMouseMove)
        self.Bind(wx.EVT_RIGHT_UP,    self.OnExit)
        self.Bind(wx.EVT_KEY_UP,      self.OnKey)
        self.Bind(wx.EVT_PAINT,       self.OnPaint)

        image = wx.Image('Meanwhile-in-England.jpg')
        self.bmp = image.Scale(500, 200).ConvertToBitmap()
        self.w, self.h = self.bmp.GetWidth(), self.bmp.GetHeight()
        self.SetClientSize( (self.w, self.h) )

        if wx.Platform != "__WXMAC__":
            # wxMac clips the tooltip to the window shape, YUCK!!!
            self.SetToolTip("Right-click to close the window\n"
                            "Double-click the image to set/unset the window shape")

        if wx.Platform == "__WXGTK__":
            # wxGTK requires that the window be created before you can
            # set its shape, so delay the call to SetWindowShape until
            # this event.
            self.Bind(wx.EVT_WINDOW_CREATE, self.SetWindowShape)
        else:
            # On wxMSW and wxMac the window has already been created, so go for it.
            self.SetWindowShape()

        # dc = wx.ClientDC(self)
        # dc.DrawBitmap(self.bmp, 0, 0, True)
        self.Centre()


    def SetWindowShape(self, *evt):
        # Use the bitmap's mask to determine the region
        r = wx.Region(self.bmp)
        self.hasShape = self.SetShape(r)

    def OnDoubleClick(self, evt):
        if self.hasShape:
            self.SetShape(wx.Region())
            self.hasShape = False
        else:
            self.SetWindowShape()

    def OnPaint(self, evt):
        dc = wx.PaintDC(self)
        dc.DrawBitmap(self.bmp, 0, 0, True)

    def OnExit(self, evt):
        self.Close()

    def OnLeftDown(self, evt):
        self.CaptureMouse()
        x, y = self.ClientToScreen(evt.GetPosition())
        originx, originy = self.GetPosition()
        dx = x - originx
        dy = y - originy
        self.delta = ((dx, dy))

    def OnLeftUp(self, evt):
        if self.HasCapture():
            self.ReleaseMouse()

    def OnMouseMove(self, evt):
        if evt.Dragging() and evt.LeftIsDown():
            x, y = self.ClientToScreen(evt.GetPosition())
            fp = (x - self.delta[0], y - self.delta[1])
            self.Move(fp)

    def OnKey(self, evt):
        kc = evt.GetKeyCode()
        if kc == wx.WXK_ESCAPE:
            self.Close()

        if kc == wx.WXK_UP:
            self.w += 50
            self.h += 50
            self.SetClientSize( (self.w, self.h) )

        if kc == wx.WXK_DOWN:
            self.w -= 50
            self.h -= 50
            self.SetClientSize( (self.w, self.h) )


if __name__ == '__main__':
    import sys,os
    # run.main(['', os.path.basename(sys.argv[0])] + sys.argv[1:])

    app = wx.App()
    # img = wx.Bitmap('Meanwhile-in-England.jpg')
    # r = wx.Region(img)
    # print(r)
    frm = TestFrame(None, sys.argv, title='Hello World 2')
    frm.Show()
    app.MainLoop()
